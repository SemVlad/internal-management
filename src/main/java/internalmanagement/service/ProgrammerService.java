package internalmanagement.service;

import internalmanagement.entity.Programmer;
import internalmanagement.enummodel.LevelEnum;
import internalmanagement.enummodel.SpecializationEnum;

import java.util.List;

public interface ProgrammerService {

    List<Programmer> findAllProgrammers();

    Programmer findProgrammerById(Integer id);

    Programmer addProgrammer(Programmer programmer);

    void deleteProgrammer(Integer id);

    Programmer updatePhone(Integer id, String phone);

    Programmer updateProgrammerLevel(Integer id, LevelEnum level);

    Programmer updateProgrammerSpecialization(Integer id, SpecializationEnum specialization);
}
