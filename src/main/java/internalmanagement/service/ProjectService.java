package internalmanagement.service;

import internalmanagement.entity.Manager;
import internalmanagement.entity.Programmer;
import internalmanagement.entity.Project;

import java.util.List;

public interface ProjectService {

    List<Project> findAllProjects();

    Project findProjectById(Integer id);

    Project addProject(Project project);

    void deleteProject(Integer id);

    Project updateProjectName(Integer id, String name);

    Project updateProjectDellManager(Integer id, Integer idManager);

    Project updateProjectAddManager(Integer id, Manager manager);

    Project updateProjectDellProgrammer(Integer id, Integer idProgrammer);

    Project updateProjectAddProgrammer(Integer id, Programmer programmer);
}
