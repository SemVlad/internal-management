package internalmanagement.service;

import internalmanagement.entity.Manager;

import java.util.List;

public interface ManagerService {

    List<Manager> findAllManagers();

    Manager findManagerById(Integer id);

    Manager addManager(Manager manager);

    void deleteManager(Integer id);

    Manager updatePhone(Integer id, String phone);
}
