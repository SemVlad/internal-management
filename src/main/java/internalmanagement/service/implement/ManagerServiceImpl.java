package internalmanagement.service.implement;

import internalmanagement.entity.Manager;
import internalmanagement.exception.ManagerNotFoundExc;
import internalmanagement.repository.ManagerRepository;
import internalmanagement.service.ManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ManagerServiceImpl implements ManagerService {

    private final ManagerRepository managerRepository;

    @Override
    public List<Manager> findAllManagers() {
        return managerRepository.findAll();
    }

    @Override
    public Manager findManagerById(Integer id) {
        return managerRepository.findById(id)
                .orElseThrow(() -> new ManagerNotFoundExc(id));
    }

    @Override
    public Manager addManager(Manager manager) {
        return managerRepository.save(manager);
    }

    @Override
    public void deleteManager(Integer id) {
        managerRepository.deleteById(id);
    }

    @Override
    public Manager updatePhone(Integer id, String phone) {
        return managerRepository.findById(id).map(manager -> {
                    manager.setPhone(phone);
                    managerRepository.save(manager);
                    return manager;
                })
                .orElseThrow(() -> new ManagerNotFoundExc(id));
    }
}

