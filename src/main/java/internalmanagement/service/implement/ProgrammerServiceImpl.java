package internalmanagement.service.implement;

import internalmanagement.entity.Programmer;
import internalmanagement.enummodel.LevelEnum;
import internalmanagement.enummodel.SpecializationEnum;
import internalmanagement.exception.ProgrammerNotFoundExc;
import internalmanagement.exception.ProjectNotFoundExc;
import internalmanagement.repository.ProgrammerRepository;
import internalmanagement.service.ProgrammerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProgrammerServiceImpl implements ProgrammerService {

    private final ProgrammerRepository programmerRepository;

    @Override
    public List<Programmer> findAllProgrammers() {
        return programmerRepository.findAll();
    }

    @Override
    public Programmer findProgrammerById(Integer id) {
        return programmerRepository.findById(id)
                .orElseThrow(() -> new ProgrammerNotFoundExc(id));
    }

    @Override
    public Programmer addProgrammer(Programmer programmer) {
        return programmerRepository.save(programmer);
    }

    @Override
    public void deleteProgrammer(Integer id) {
        programmerRepository.deleteById(id);
    }

    @Override
    public Programmer updatePhone(Integer id, String phone) {
        return programmerRepository.findById(id).map(programmer -> {
                    programmer.setPhone(phone);
                    programmerRepository.save(programmer);
                    return programmer;
                })
                .orElseThrow(() -> new ProgrammerNotFoundExc(id));
    }

    @Override
    public Programmer updateProgrammerLevel(Integer id, LevelEnum level) {
        return programmerRepository.findById(id).map(programmer -> {
                    programmer.setLevel(level);
                    programmerRepository.save(programmer);
                    return programmer;
                })
                .orElseThrow(() -> new ProgrammerNotFoundExc(id));
    }

    @Override
    public Programmer updateProgrammerSpecialization(Integer id, SpecializationEnum specialization) {
        return programmerRepository.findById(id).map(programmer -> {
                    programmer.setSpecialization(specialization);
                    programmerRepository.save(programmer);
                    return programmer;
                })
                .orElseThrow(() -> new ProjectNotFoundExc(id));
    }
}

