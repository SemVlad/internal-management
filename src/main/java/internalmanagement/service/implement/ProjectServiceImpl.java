package internalmanagement.service.implement;

import internalmanagement.entity.Manager;
import internalmanagement.entity.Programmer;
import internalmanagement.entity.Project;
import internalmanagement.exception.ProjectNotFoundExc;
import internalmanagement.repository.ProjectRepository;
import internalmanagement.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    @Override
    public List<Project> findAllProjects() {
        return projectRepository.findAll();
    }

    @Override
    public Project findProjectById(Integer id) {
        return projectRepository.findById(id)
                .orElseThrow(() -> new ProjectNotFoundExc(id));
    }

    @Override
    public Project addProject(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public void deleteProject(Integer id) {
        projectRepository.deleteById(id);
    }

    @Override
    public Project updateProjectName(Integer id, String name) {
        return projectRepository.findById(id).map(project -> {
                    project.setName(name);
                    projectRepository.save(project);
                    return project;
                })
                .orElseThrow(() -> new ProjectNotFoundExc(id));
    }

    @Override
    public Project updateProjectDellManager(Integer idProject, Integer idManager) {
        return projectRepository.findById(idProject).map(project -> {
                    project.setManagerList(project.getManagerList()
                            .stream().filter(manager -> manager.getId() != idManager)
                            .collect(Collectors.toList()));
                    return projectRepository.save(project);
                })
                .orElseThrow(() -> new ProjectNotFoundExc(idProject));
    }

    @Override
    public Project updateProjectAddManager(Integer idProject, Manager manager) {
        return projectRepository.findById(idProject).map(project -> {
                    project.getManagerList().add(manager);
                    return projectRepository.save(project);
                })
                .orElseThrow(() -> new ProjectNotFoundExc(idProject));
    }

    @Override
    public Project updateProjectDellProgrammer(Integer idProject, Integer idProgrammer) {
        return projectRepository.findById(idProject).map(project -> {
                    project.setProgrammerList(project.getProgrammerList()
                            .stream().filter(programmer -> programmer.getId() != idProgrammer)
                            .collect(Collectors.toList()));
                    return projectRepository.save(project);
                })
                .orElseThrow(() -> new ProjectNotFoundExc(idProject));
    }

    @Override
    public Project updateProjectAddProgrammer(Integer idProject, Programmer programmer) {
        return projectRepository.findById(idProject).map(project -> {
                    project.getProgrammerList().add(programmer);
                    return projectRepository.save(project);
                })
                .orElseThrow(() -> new ProjectNotFoundExc(idProject));
    }
}
