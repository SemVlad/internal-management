package internalmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InternalManagement {
    public static void main(String[] args) {
        SpringApplication.run(InternalManagement.class, args);
    }
}
