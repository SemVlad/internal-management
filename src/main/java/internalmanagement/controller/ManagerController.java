package internalmanagement.controller;

import internalmanagement.entity.Manager;
import internalmanagement.service.ManagerService;
import internalmanagement.service.implement.ManagerServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/managers")
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    @GetMapping("/")
    List<Manager> getAllManagers() {
        return managerService.findAllManagers();
    }

    @GetMapping("/{id}")
    Manager getManagerById(@PathVariable Integer id) {
        return managerService.findManagerById(id);
    }

    @PutMapping("/{id}/phone")
    Manager updatePhone(@PathVariable Integer id, @RequestBody String phone) {
        return managerService.updatePhone(id, phone);
    }

    @PostMapping("/")
    Manager addManager(@RequestBody Manager manager) {
        return managerService.addManager(manager);
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteManager(@PathVariable Integer id) {
        managerService.deleteManager(id);
        return new ResponseEntity("Manager with id= " + id
                + "has been deleted ", HttpStatus.valueOf(204));
    }
}
