package internalmanagement.controller;

import internalmanagement.entity.Programmer;
import internalmanagement.enummodel.LevelEnum;
import internalmanagement.enummodel.SpecializationEnum;
import internalmanagement.service.ProgrammerService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/programmers")
public class ProgrammerController {

    @Autowired
    private ProgrammerService programmerService;

    @GetMapping("/")
    List<Programmer> getAllProgrammers() {
        return programmerService.findAllProgrammers();
    }

    @GetMapping("/{id}")
    Programmer getProgrammerById(@PathVariable Integer id) {
        return programmerService.findProgrammerById(id);
    }

    @PostMapping("/")
    Programmer addProgrammer(@RequestBody Programmer programmer) {
        return programmerService.addProgrammer(programmer);
    }

    @PutMapping("/{id}/phone")
    Programmer updatePhone(@PathVariable Integer id, @RequestBody String phone) {
        return programmerService.updatePhone(id, phone);
    }

    @PutMapping("/{id}/level")
    Programmer updateLevel(@PathVariable Integer id, @RequestBody LevelEnum level) {
        return programmerService.updateProgrammerLevel(id, level);
    }

    @PutMapping("/{id}/specialization")
    Programmer updateSpecialization(@PathVariable Integer id, @RequestBody SpecializationEnum specialization) {
        return programmerService.updateProgrammerSpecialization(id, specialization);
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteProgrammer(@PathVariable Integer id) {
        programmerService.deleteProgrammer(id);
        return new ResponseEntity("Programmer with id= " + id
                + "has been deleted ", HttpStatus.valueOf(204));
    }
}
