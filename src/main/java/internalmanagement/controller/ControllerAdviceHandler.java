package internalmanagement.controller;

import internalmanagement.exception.ManagerNotFoundExc;
import internalmanagement.exception.ProgrammerNotFoundExc;
import internalmanagement.exception.ProjectNotFoundExc;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerAdviceHandler {

    @ExceptionHandler(value = {ManagerNotFoundExc.class})
    public ResponseEntity<String> handleNotFoundExceptionManager(RuntimeException exception) {
        return new ResponseEntity<>("Search results : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {ProgrammerNotFoundExc.class})
    public ResponseEntity<String> handleNotFoundExceptionProgrammer(RuntimeException exception) {
        return new ResponseEntity<>("Search results : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {ProjectNotFoundExc.class})
    public ResponseEntity<String> handleNotFoundExceptionProject(RuntimeException exception) {
        return new ResponseEntity<>("Search results : " + exception.getMessage(), HttpStatus.NOT_FOUND);
    }
}
