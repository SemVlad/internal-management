package internalmanagement.controller;

import internalmanagement.entity.Manager;
import internalmanagement.entity.Programmer;
import internalmanagement.entity.Project;
import internalmanagement.service.ProjectService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/")
    List<Project> getAllProjects() {
        return projectService.findAllProjects();
    }

    @GetMapping("/{id}")
    Project getProjectById(@PathVariable Integer id) {
        return projectService.findProjectById(id);
    }

    @PostMapping("/")
    Project addProject(@RequestBody Project project) {
        return projectService.addProject(project);
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteProject(@PathVariable Integer id) {
        projectService.deleteProject(id);
        return new ResponseEntity("Project with id= " + id
                + "has been deleted ", HttpStatus.valueOf(204));
    }

    @PutMapping("/{id}/name")
    Project updateProjectName(@PathVariable Integer id, @RequestBody String name) {
        return projectService.updateProjectName(id, name);
    }

    @PutMapping("/{id}/removeManager")
    Project updateProjectDellManager(@PathVariable Integer id, @RequestBody Integer idManager) {
        return projectService.updateProjectDellManager(id, idManager);
    }

    @PutMapping("/{id}/addManager")
    Project updateProjectAddManager(@PathVariable Integer id, @RequestBody Manager manager) {
        return projectService.updateProjectAddManager(id, manager);
    }

    @PutMapping("/{id}/removeProgrammer")
    Project updateProjectDellProgrammer(@PathVariable Integer id, @RequestBody Integer idProgrammer) {
        return projectService.updateProjectDellProgrammer(id, idProgrammer);
    }

    @PutMapping("/{id}/addProgrammer")
    Project updateProjectAddProgrammer(@PathVariable Integer id, @RequestBody Programmer programmer) {
        return projectService.updateProjectAddProgrammer(id, programmer);
    }
}
