package internalmanagement.repository;

import internalmanagement.entity.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ManagerRepository extends JpaRepository<Manager, Integer> {

    @Override
    Optional<Manager> findById(Integer integer);
}
