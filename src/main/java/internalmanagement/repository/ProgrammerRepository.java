package internalmanagement.repository;

import internalmanagement.entity.Programmer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProgrammerRepository extends JpaRepository<Programmer, Integer> {

    @Override
    Optional<Programmer> findById(Integer integer);
}
