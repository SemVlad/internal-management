package internalmanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import internalmanagement.enummodel.LevelEnum;
import internalmanagement.enummodel.SpecializationEnum;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "programmer")
public class Programmer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String surname;
    private int age;
    private String phone;

    @Column(name = "level")
    @Enumerated(EnumType.STRING)
    private LevelEnum level;

    @Column(name = "specialization")
    @Enumerated(EnumType.STRING)
    private SpecializationEnum specialization;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    }, fetch = FetchType.EAGER,
            mappedBy = "managerList")
    @JsonIgnore
    private List<Project> projectList;
}
