package internalmanagement.exception;

import lombok.Data;

@Data
public class ProgrammerNotFoundExc extends RuntimeException{
    public ProgrammerNotFoundExc(Integer id) {
        super("Programmer with id = " + id + " not found");
    }
}
