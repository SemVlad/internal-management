package internalmanagement.exception;

import lombok.Data;

@Data
public class ProjectNotFoundExc extends RuntimeException {
    public ProjectNotFoundExc(Integer id) {
        super("Project with id = " + id + " not found");
    }
}
