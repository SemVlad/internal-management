package internalmanagement.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ManagerNotFoundExc extends RuntimeException {

    public ManagerNotFoundExc(Integer id) {
        super("Manager with id = " + id + " not found");
    }
}
