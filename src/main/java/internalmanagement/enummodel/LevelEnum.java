package internalmanagement.enummodel;

public enum LevelEnum {
    JUNIOR,
    MIDDLE,
    SENIOR
}
