package internalmanagement.enummodel;

public enum SpecializationEnum {
    QA,
    DEVELOPER,
    DEVOPS
}
