package internalmanagement.service.implement;

import internalmanagement.entity.Manager;
import internalmanagement.repository.ManagerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class ManagerServiceImplTest {
    @Mock
    private ManagerRepository managerRepository;
    @InjectMocks
    private ManagerServiceImpl managerService;
    private Manager manager;

    @Before
    public void setUp() {
        managerService = new ManagerServiceImpl(managerRepository);
        manager = new Manager(1, "Valya", "Ivanov", 23, "2323424", new ArrayList<>());
    }

    @Test
    public void findAllManagers() {
        Mockito.when(managerRepository.findAll()).thenReturn(List.of(manager));
        List<Manager> managers = managerService.findAllManagers();
        assertEquals(1, managers.size());
        assertEquals(manager, managers.get(0));
        verify(managerRepository).findAll();
    }

    @Test
    public void findManagerById() {
        Mockito.when(managerRepository.findById(1)).thenReturn(Optional.ofNullable(manager));
        Manager man = managerService.findManagerById(1);
        assertEquals("Valya", man.getName());
        verify(managerRepository).findById(1);
    }

    @Test
    public void addManager() {
        Mockito.when(managerRepository.save(manager)).thenReturn(manager);
        Manager man = new Manager(2, "Kolya", "Ivanov", 26, "4543534", new ArrayList<>());
        Manager manager2 = managerService.addManager(man);
        List<Manager> list = new ArrayList<>();
        list.add(manager2);
        list.add(manager);
        assertEquals(2, list.size());
        assertEquals(manager2, list.get(0));
    }

    @Test
    public void deleteManager() {
        Mockito.when(managerRepository.findById(manager.getId())).thenReturn(Optional.ofNullable(manager));
        managerService.deleteManager(manager.getId());
        verify(managerRepository, times(1)).deleteById(manager.getId());
    }

    @Test
    public void updatePhone() {
        Mockito.when(managerRepository.findById(manager.getId())).thenReturn(Optional.ofNullable(manager));
        Mockito.when(managerRepository.save(any())).thenReturn(manager);
        managerService.updatePhone(manager.getId(), "00000");
        assertEquals("00000", manager.getPhone());
    }
}