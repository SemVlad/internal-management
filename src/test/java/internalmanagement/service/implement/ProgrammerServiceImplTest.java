package internalmanagement.service.implement;

import internalmanagement.entity.Programmer;
import internalmanagement.repository.ProgrammerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static internalmanagement.enummodel.LevelEnum.*;
import static internalmanagement.enummodel.SpecializationEnum.DEVOPS;
import static internalmanagement.enummodel.SpecializationEnum.QA;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class ProgrammerServiceImplTest {
    @Mock
    private ProgrammerRepository programmerRepository;
    @InjectMocks
    private ProgrammerServiceImpl programmerService;
    private Programmer programmer;

    @Before
    public void setUp() {
        programmerService = new ProgrammerServiceImpl(programmerRepository);
        programmer = new Programmer(1, "Vanya", "Ivanovich", 23,
                "1232435", JUNIOR, DEVOPS, new ArrayList<>());
    }

    @Test
    public void findAllProgrammers() {
        Mockito.when(programmerRepository.findAll()).thenReturn(List.of(programmer));
        List<Programmer> programmers = programmerService.findAllProgrammers();
        assertEquals(1, programmers.size());
        assertEquals(programmer, programmers.get(0));
    }

    @Test
    public void findProgrammerById() {
        Mockito.when(programmerRepository.findById(1)).thenReturn(Optional.ofNullable(programmer));
        Programmer programmer = programmerService.findProgrammerById(1);
        assertEquals(1, programmer.getId());
        assertEquals("Vanya", programmer.getName());
    }

    @Test
    public void addProgrammer() {
        Programmer pr = new Programmer(2, "Van", "Ivan", 233,
                "11111", MIDDLE, QA, new ArrayList<>());
        Mockito.when(programmerRepository.save(programmer)).thenReturn(programmer);
        Programmer programmerTwo = programmerService.addProgrammer(pr);
        List<Programmer> list = new ArrayList<>();
        list.add(programmer);
        list.add(programmerTwo);
        assertEquals(2, list.size());
        assertEquals(programmer, list.get(0));
    }

    @Test
    public void deleteProgrammer() {
        Mockito.when(programmerRepository.findById(programmer.getId())).thenReturn(Optional.ofNullable(programmer));
        programmerService.deleteProgrammer(programmer.getId());
        verify(programmerRepository, times(1)).deleteById(programmer.getId());
    }

    @Test
    public void updatePhone() {
        Mockito.when(programmerRepository.findById(programmer.getId())).thenReturn(Optional.ofNullable(programmer));
        Mockito.when(programmerRepository.save(any())).thenReturn(programmer);
        programmerService.updatePhone(programmer.getId(), "2222222");
        assertEquals("2222222", programmer.getPhone());
        verify(programmerRepository).findById(any());
        verify(programmerRepository).save(any());
    }

    @Test
    public void updateProgrammerLevel() {
        Mockito.when(programmerRepository.findById(programmer.getId())).thenReturn(Optional.ofNullable(programmer));
        Mockito.when(programmerRepository.save(any())).thenReturn(programmer);
        programmerService.updateProgrammerLevel(programmer.getId(), SENIOR);
        assertEquals(SENIOR, programmer.getLevel());
        verify(programmerRepository).findById(any());
        verify(programmerRepository).save(any());
    }

    @Test
    public void updateProgrammerSpecialization() {
        Mockito.when(programmerRepository.findById(programmer.getId())).thenReturn(Optional.ofNullable(programmer));
        Mockito.when(programmerRepository.save(any())).thenReturn(programmer);
        programmerService.updateProgrammerSpecialization(programmer.getId(), QA);
        assertEquals(QA, programmer.getSpecialization());
        verify(programmerRepository).findById(any());
        verify(programmerRepository).save(any());
    }
}