package internalmanagement.controller;

import internalmanagement.entity.Manager;
import internalmanagement.repository.ManagerRepository;
import internalmanagement.service.implement.ManagerServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class ManagerControllerTest {
    @Mock
    private ManagerRepository managerRepository;
    @Mock
    private ManagerServiceImpl managerService;
    private Manager manager;
    @InjectMocks
    private ManagerController managerController;


    @Before
    public void setUp() {
        managerService = new ManagerServiceImpl(managerRepository);
        managerController = new ManagerController(managerService);
        manager = new Manager(1, "Valya", "Ivanov", 23, "2323424", new ArrayList<>());
    }

    @Test
    public void getAllManagers() {
        Mockito.when(managerRepository.findAll()).thenReturn(List.of(manager));
        List<Manager> allManagers = managerController.getAllManagers();
        assertEquals(1, allManagers.size());
        assertEquals("Valya", allManagers.get(0).getName());
        verify(managerRepository).findAll();
    }

    @Test
    public void getManagerById() {
        Mockito.when(managerRepository.findById(any())).thenReturn(Optional.ofNullable(manager));
        Manager manager = managerController.getManagerById(any());
        assertEquals("Valya", manager.getName());
        verify(managerRepository).findById(any());
    }

    @Test
    public void updatePhone() {
        Mockito.when(managerRepository.findById(manager.getId())).thenReturn(Optional.ofNullable(manager));
        managerController.updatePhone(manager.getId(), "00000");
        assertEquals("00000", manager.getPhone());
        verify(managerRepository).findById(any());
        verify(managerRepository).save(any());
    }

    @Test
    public void addManager() {
        Mockito.when(managerRepository.save(manager)).thenReturn(manager);
        Manager man = new Manager(2, "Kolya", "Ivanov", 26, "4543534", new ArrayList<>());
        Manager manager2 = managerController.addManager(man);
        List<Manager> list = new ArrayList<>();
        list.add(manager2);
        list.add(manager);
        assertEquals(2, list.size());
        assertEquals(manager2, list.get(0));
        verify(managerRepository).save(any());
    }

    @Test
    public void deleteManager() {
        Mockito.when(managerRepository.findById(manager.getId())).thenReturn(Optional.ofNullable(manager));
        managerController.deleteManager(manager.getId());
        verify(managerRepository, times(1)).deleteById(manager.getId());
    }
}