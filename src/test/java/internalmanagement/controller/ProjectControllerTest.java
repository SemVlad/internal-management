package internalmanagement.controller;

import internalmanagement.entity.Manager;
import internalmanagement.entity.Programmer;
import internalmanagement.entity.Project;
import internalmanagement.repository.ProjectRepository;
import internalmanagement.service.implement.ProjectServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static internalmanagement.enummodel.LevelEnum.MIDDLE;
import static internalmanagement.enummodel.SpecializationEnum.DEVELOPER;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class ProjectControllerTest {
    @Mock
    private ProjectRepository projectRepository;
    @Mock
    private ProjectServiceImpl projectService;
    private Project project;
    @InjectMocks
    private ProjectController projectController;

    @Before
    public void setUp() {
        Manager managerOne = new Manager(1, "Ivan", "Ivanov", 2,
                "2323", new ArrayList<>());
        Manager managerTwo = new Manager();
        List<Manager> managerList = new ArrayList<>();
        managerList.add(managerOne);
        managerList.add(managerTwo);

        Programmer programmerOne = new Programmer(1, "Pavlo", "Pavlov", 3,
                "90909009", MIDDLE, DEVELOPER, new ArrayList<>());
        Programmer programmerTwo = new Programmer();
        List<Programmer> programmerList = new ArrayList<>();
        programmerList.add(programmerOne);
        programmerList.add(programmerTwo);

        projectService = new ProjectServiceImpl(projectRepository);
        projectController = new ProjectController(projectService);
        project = new Project(1, "space", managerList, programmerList);
    }

    @Test
    public void getAllProjects() {
        Mockito.when(projectRepository.findAll()).thenReturn(List.of(project));
        List<Project> allProjects = projectController.getAllProjects();
        assertEquals(1, allProjects.size());
        assertEquals("space", allProjects.get(0).getName());
        verify(projectRepository).findAll();
    }

    @Test
    public void getProjectById() {
        Mockito.when(projectRepository.findById(any())).thenReturn(Optional.ofNullable(project));
        Project project = projectController.getProjectById(any());
        assertEquals("space", project.getName());
        verify(projectRepository).findById(any());
    }

    @Test
    public void addProject() {
        Mockito.when(projectRepository.save(project)).thenReturn(project);
        Project proj = new Project(2, "BMV", new ArrayList<>(), new ArrayList<>());
        Project project2 = projectController.addProject(proj);
        List<Project> list = new ArrayList<>();
        list.add(project2);
        list.add(proj);
        assertEquals(2, list.size());
        assertEquals(project2, list.get(0));
        verify(projectRepository).save(any());
    }

    @Test
    public void deleteProject() {
        Mockito.when(projectRepository.findById(project.getId())).thenReturn(Optional.ofNullable(project));
        projectController.deleteProject(project.getId());
        verify(projectRepository, times(1)).deleteById(project.getId());
    }

    @Test
    public void updateProjectName() {
        Mockito.when(projectRepository.findById(project.getId())).thenReturn(Optional.ofNullable(project));
        projectController.updateProjectName(project.getId(), "BIG");
        assertEquals("BIG", project.getName());
        assertEquals(1, project.getId());
        verify(projectRepository).findById(any());
        verify(projectRepository).save(any());
    }

    @Test
    public void updateProjectDellManager() {
        Mockito.when(projectRepository.findById(any())).thenReturn(Optional.ofNullable(project));
        Mockito.when(projectRepository.save(any())).thenReturn(project);
        Project projectUpdated = projectController.updateProjectDellManager(1, 1);
        assertEquals(1, projectUpdated.getManagerList().size());
        verify(projectRepository).findById(any());
        verify(projectRepository).save(any());
    }

    @Test
    public void updateProjectAddManager() {
        Manager manager = new Manager(1, "Max", "Ivanov", 2, "2323", new ArrayList<>());
        Mockito.when(projectRepository.findById(any())).thenReturn(Optional.ofNullable(project));
        Mockito.when(projectRepository.save(any())).thenReturn(project);
        Project projectUpdated = projectController.updateProjectAddManager(1, manager);
        assertEquals(3, projectUpdated.getManagerList().size());
        assertEquals("Max", projectUpdated.getManagerList().get(2).getName());
        verify(projectRepository).findById(any());
        verify(projectRepository).save(any());
    }

    @Test
    public void updateProjectDellProgrammer() {
        Mockito.when(projectRepository.findById(any())).thenReturn(Optional.ofNullable(project));
        Mockito.when(projectRepository.save(any())).thenReturn(project);
        Project projectUpdated = projectController.updateProjectDellProgrammer(1, 1);
        assertEquals(1, projectUpdated.getProgrammerList().size());
        verify(projectRepository).findById(any());
        verify(projectRepository).save(any());
    }

    @Test
    public void updateProjectAddProgrammer() {
        Programmer programmer = new Programmer(1, "Vova", "Sergeev", 3,
                "90909009", MIDDLE, DEVELOPER, new ArrayList<>());
        Mockito.when(projectRepository.findById(any())).thenReturn(Optional.ofNullable(project));
        Mockito.when(projectRepository.save(any())).thenReturn(project);
        Project projectUpdated = projectController.updateProjectAddProgrammer(1, programmer);
        assertEquals(3, projectUpdated.getProgrammerList().size());
        assertEquals("Vova", projectUpdated.getProgrammerList().get(2).getName());
        verify(projectRepository).findById(any());
        verify(projectRepository).save(any());
    }
}