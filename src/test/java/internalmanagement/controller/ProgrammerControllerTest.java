package internalmanagement.controller;

import internalmanagement.entity.Programmer;
import internalmanagement.repository.ProgrammerRepository;
import internalmanagement.service.implement.ProgrammerServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static internalmanagement.enummodel.LevelEnum.*;
import static internalmanagement.enummodel.LevelEnum.SENIOR;
import static internalmanagement.enummodel.SpecializationEnum.DEVOPS;
import static internalmanagement.enummodel.SpecializationEnum.QA;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class ProgrammerControllerTest {
    @Mock
    private ProgrammerRepository programmerRepository;
    @Mock
    private ProgrammerServiceImpl programmerService;
    private Programmer programmer;
    @InjectMocks
    private ProgrammerController programmerController;

    @Before
    public void setUp() {
        programmerService = new ProgrammerServiceImpl(programmerRepository);
        programmerController = new ProgrammerController(programmerService);
        programmer = new Programmer(1, "Vanya", "Ivanovich", 23,
                "1232435", JUNIOR, DEVOPS, new ArrayList<>());
    }

    @Test
    public void getAllProgrammers() {
        Mockito.when(programmerRepository.findAll()).thenReturn(List.of(programmer));
        List<Programmer> allProgrammers = programmerController.getAllProgrammers();
        assertEquals(1, allProgrammers.size());
        assertEquals("Vanya", allProgrammers.get(0).getName());
        verify(programmerRepository).findAll();
    }

    @Test
    public void getProgrammerById() {
        Mockito.when(programmerRepository.findById(any())).thenReturn(Optional.ofNullable(programmer));
        Programmer programmer = programmerController.getProgrammerById(any());
        assertEquals("Vanya", programmer.getName());
        verify(programmerRepository).findById(any());
    }

    @Test
    public void addProgrammer() {
        Programmer pr = new Programmer(2, "Van", "Ivan", 233,
                "11111", MIDDLE, QA, new ArrayList<>());
        Mockito.when(programmerRepository.save(programmer)).thenReturn(programmer);
        Programmer programmerTwo = programmerController.addProgrammer(pr);
        List<Programmer> list = new ArrayList<>();
        list.add(programmer);
        list.add(programmerTwo);
        assertEquals(2, list.size());
        assertEquals(programmer, list.get(0));
        verify(programmerRepository).save(any());
    }

    @Test
    public void updatePhone() {
        Mockito.when(programmerRepository.findById(programmer.getId())).thenReturn(Optional.ofNullable(programmer));
        programmerController.updatePhone(programmer.getId(), "2222222");
        assertEquals("2222222", programmer.getPhone());
        verify(programmerRepository).findById(any());
        verify(programmerRepository).save(any());
    }

    @Test
    public void updateLevel() {
        Mockito.when(programmerRepository.findById(programmer.getId())).thenReturn(Optional.ofNullable(programmer));
        programmerController.updateLevel(programmer.getId(), SENIOR);
        assertEquals(SENIOR, programmer.getLevel());
        verify(programmerRepository).findById(any());
        verify(programmerRepository).save(any());
    }

    @Test
    public void updateSpecialization() {
        Mockito.when(programmerRepository.findById(programmer.getId())).thenReturn(Optional.ofNullable(programmer));
        programmerController.updateSpecialization(programmer.getId(), QA);
        assertEquals(QA, programmer.getSpecialization());
        verify(programmerRepository).findById(any());
        verify(programmerRepository).save(any());
    }

    @Test
    public void deleteProgrammer() {
        Mockito.when(programmerRepository.findById(programmer.getId())).thenReturn(Optional.ofNullable(programmer));
        programmerController.deleteProgrammer(programmer.getId());
        verify(programmerRepository, times(1)).deleteById(programmer.getId());
    }
}